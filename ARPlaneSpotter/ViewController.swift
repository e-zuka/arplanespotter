//
//  ViewController.swift
//  ARPlaneSpotter
//
//  Created by 飯塚淳 on 2019/06/24.
//  Copyright © 2019 Atsushi Iizuka. All rights reserved.
//

import UIKit
import RealityKit
import ARKit

class ViewController: UIViewController {
    
    @IBOutlet var arView: ARView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arView.session.delegate = self
        // Load the "Box" scene from the "Experience" Reality File
        let landingAnchor = try! Spotter.loadLanding()
        //landingAnchor.components[AnchoringComponent] = AnchoringComponent(.plane(.horizontal, classification: .any, minimumBounds: [0.2, 0.2]))

        // Add the box anchor to the scene
        arView.scene.anchors.append(landingAnchor)

        guard let a359 = landingAnchor.a359 else { return }
        let originalPoisition = a359.transform
        
        landingAnchor.actions.landed.onAction = { entity in
            a359.transform = originalPoisition
            landingAnchor.notifications.clearToLand.post()
        }
        
        landingAnchor.notifications.clearToLand.post()
    }
}

extension ViewController : ARSessionDelegate {
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
    }
    
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        for anchor in anchors {
            if let planeAnchor = anchor as? ARPlaneAnchor {
                print(planeAnchor.transform)
            }
        }
    }
}
